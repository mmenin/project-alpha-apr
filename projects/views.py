from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):

    projects_list = Project.objects.filter(owner=request.user)

    context = {
        "projects_list": projects_list,
    }
    return render(request, "projects/proj_list.html", context)


@login_required
def show_project(request, pk):

    project = get_object_or_404(Project, id=pk)

    context = {
        "project": project,
    }
    return render(request, "projects/proj_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()

            return redirect("list_projects")

        else:
            form = ProjectForm()

    else:
        form = ProjectForm()
        context = {"form": form}

        return render(request, "projects/proj_create.html", context)
